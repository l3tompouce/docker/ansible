FROM python:alpine

LABEL maintainer="ToM <tom@leloop.org>"

# build env
ARG ANSIBLE_VERSION=2.9.*
ARG ANSIBLELINT_VERSION=5.4.0

ENV ANSIBLE_USER=ansible
ENV ANSIBLE_HOME=/home/ansible
ENV ANSIBLE_UID=1000

# hadolint ignore=DL3013,DL3018
RUN echo "[+] Install build time deps..." \
    && apk add --no-cache --virtual .build-deps \
        gcc \
        libffi-dev \
        linux-headers \
        make \
        musl-dev \
        openssl-dev \
    && echo "[+] Install Python libraries..." \
    && pip install --no-cache-dir --upgrade --root-user-action=ignore \
        pip \
    && pip install --no-cache-dir --root-user-action=ignore \
        paramiko \
    \
    \
    && echo "[+] Install Ansible..." \
    && pip install --no-cache-dir --root-user-action=ignore \
        ansible==${ANSIBLE_VERSION} \
        ansible-lint==${ANSIBLELINT_VERSION} \
    \
    \
    && echo "[+] Install run time dependencies..." \
    && apk add --no-cache \
        bash \
        ca-certificates \
        curl \
        git \
        openssl \
        sudo \
        tzdata \
    \
    \
    && echo "[+] Install a few optional tools..." \
    && apk add --no-cache \
        openssh-client \
        sshpass \
    && pip install --no-cache-dir --root-user-action=ignore \
        netaddr \
        requests \
    \
    \
    && echo "[+] Remove build time deps..." \
    && apk del .build-deps \
    \
    \
    && echo "[+] Add ansible user..." \
    && adduser \
        -h "${ANSIBLE_HOME}" \
        -s /sbin/nologin \
        -u "${ANSIBLE_UID}" \
        -D "${ANSIBLE_USER}" \
        "${ANSIBLE_USER}" \
    \
    \
    && echo "[+] Add ansible inventory for convenience..." \
    && mkdir -p /etc/ansible \
    && printf '[local]\nlocalhost\n' > /etc/ansible/hosts \
    \
    \
    && echo "[+] Add Github/GitLab to known_hosts..." \
    && mkdir -p ${ANSIBLE_HOME}/.ssh \
    && ssh-keyscan -H github.com >> ${ANSIBLE_HOME}/.ssh/known_hosts \
    && ssh-keyscan -H gitlab.com >> ${ANSIBLE_HOME}/.ssh/known_hosts \
    && chown -R "${ANSIBLE_USER}:${ANSIBLE_USER}" "${ANSIBLE_HOME}"

WORKDIR /src
USER ${ANSIBLE_USER}
