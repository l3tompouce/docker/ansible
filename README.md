# letompouce/ansible

Ansible images for fun and profit.

Ships with:

* ansible, ansible-lint
* ssh-client, sshpass

## build

Default version: `ANSIBLE_VERSION=2.9`

Override versions (`ANSIBLE_VERSION`, `ANSIBLELINT_VERSION`):

```shell
docker build \
    --build-arg ANSIBLE_VERSION=2.5 \
    -t letompouce/ansible:2.5 \
    .
```

## run

Basic invocation:

```shell
docker run --rm letompouce/ansible ansible --version
```

`WORKDIR` is `/src/`.

Mount a local playbook:

```shell
docker run --rm \
    -v $(pwd):/src \
    letompouce/ansible \
    ansible-playbook --syntax-check playbook/site.yml
```

Use the host's timezone:

```shell
docker run --rm \
    -v /etc/localtime:/etc/localtime:ro \
    letompouce/ansible \
    ansible --version
```

Use an arbitrary timezone:

```shell
docker run --rm \
    -e 'TZ=Europe/Paris' \
    letompouce/ansible \
    ansible --version
```

## more

* Source: <https://gitlab.com/l3tompouce/docker/ansible>
* Image: <https://hub.docker.com/r/letompouce/ansible>
